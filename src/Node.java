import java.util.StringTokenizer;
import java.util.*;

// Inspired by https://enos.itcollege.ee/~jpoial/algoritmid/puud.html

public class Node {

   private String name;
   private Node firstChild;
   private Node nextSibling;
   private int info = 1;

   Node (String n, Node d, Node r) {
      this.name = n;
      this.firstChild = d;
      this.nextSibling = r;
   }

   public static Node parsePostfix (String s) {

      int depthLevel = 1;
      s = s.trim();
      List<Node> tempParents = new ArrayList<>();
      List<Node> tempChildren = new ArrayList<>();
      Node root = null;
      String nameToParse = "";


      //Checking

      boolean nextMustBeName = false;
      int maxDepth = 1;
      boolean nextMustBeNameOrOpen = false;

      if (s.toCharArray()[0] == ',' || s.toCharArray()[0] == ')') {
         throw new RuntimeException("Invalid syntax, string must start with '(' or Name of root: " + s);
      }

      StringTokenizer st = new StringTokenizer(s, "(,)", true);
      while (st.hasMoreTokens()) {
         String str = st.nextToken().trim();
         if (depthLevel < 1){
            throw new RuntimeException("Invalid syntax, check amount of '(' and ')': " + s);
         } else if ((str.equals(",") & depthLevel == 1)) {
            throw new RuntimeException("Invalid syntax, there can be only one root in tree: " + s);
         } else if (str.equals("(")){
            if (nextMustBeName) {
               throw new RuntimeException("Invalid syntax, there must be Node Name after ')': " + s);
            }
            depthLevel++;
            if (depthLevel > maxDepth){
               maxDepth = depthLevel;
            }
            nextMustBeNameOrOpen = true;
         } else if (str.equals(")")){
            if (nextMustBeName) {
               throw new RuntimeException("Invalid syntax, there must be Node Name after ')': " + s);
            } if (nextMustBeNameOrOpen) {
               throw  new RuntimeException("Invalid syntax, There cannot be empty subtree: " + s);
            }
            depthLevel--;
            nextMustBeName = true;
         } else if (str.equals(",")) {
            if (nextMustBeNameOrOpen){
               throw new RuntimeException("Invalid syntax, there must be start of subtree or Name of node after comma: " + s);
            }
            nextMustBeNameOrOpen = true;
         } else {
            if (!str.trim().equals("")) {
               nextMustBeName = false;
               nextMustBeNameOrOpen = false;
            }
            for (char ch : str.toCharArray()){
               if (ch == ' '){
                  throw new RuntimeException("Invalid syntax, there cannot be spaces in Node name: " + str);
               }
            }
         }
      } if (depthLevel > 1){
         throw new RuntimeException("Invalid syntax, check amount of '(' and ')': " + s);
      }


      //Parsing

      for (int i=1; i < maxDepth+1; i++){
         int countFamily = 0;
         StringTokenizer sT = new StringTokenizer(s, "(,)", true);
         if (i == 1){
            while (sT.hasMoreTokens()) {
               String str = sT.nextToken().trim();
               if (str.equals("(")){
                  depthLevel++;
               } if (str.equals(")")){
                  depthLevel--;
               } else {
                  if (depthLevel == 1) {
                     nameToParse = str;
                  }
               }
            }
            root = new Node(nameToParse, null, null);
            tempParents.add(root);
            nameToParse = "";
         }
         else {
            while (sT.hasMoreTokens()) {
               String str = sT.nextToken().trim();
               if (str.equals("(")){
                  depthLevel++;
               } else if (str.equals(")")){
                  depthLevel--;
               } else {
                  if (str.trim().equals("") || str.equals(",")){
                     continue;
                  }
                  if (depthLevel == i - 1){
                     countFamily++;
                  }
                  if (depthLevel == i) {
                     nameToParse = str;
                     Node newNode = new Node (nameToParse, null, null);
                     if (tempParents.get(countFamily).firstChild == null) {
                        tempParents.get(countFamily).firstChild = newNode;
                     } else {
                        tempChildren.get(tempChildren.size()-1).nextSibling = newNode;
                     }
                     tempChildren.add(newNode);
                     nameToParse = "";
                  }
               }
            }
            tempParents = tempChildren;
            tempChildren = new ArrayList<>();
         }
      }

      return root;
   }

   public String leftParentheticRepresentation() {
      StringBuilder sb = new StringBuilder();
      sb.append(this.name);
      if (this.firstChild != null){
         sb.append("(");
      }
      Node child = this.firstChild;
      while (child != null){
         sb.append(child.leftParentheticRepresentation());
         if (child.nextSibling != null) {
            sb.append(",");
         }
         child = child.nextSibling;
      }
      if (this.firstChild != null){
         sb.append(")");
      }
      return sb.toString();
   }

   public String pseudoXML(){
      StringBuilder sb = new StringBuilder();
      int count = this.info;
      String tab = "    ";

      sb.append("<L").append(count).append("> ").append(this.name);
      Node child = this.firstChild;
      Node child1 = this.firstChild;
      if (child != null){
         sb.append("\n");
         sb.append(tab.repeat(count));
         child.info = count + 1;
      } else {
         sb.append(" ");
      }
      while (child != null){
         sb.append(child.pseudoXML());
         if (child.nextSibling != null){
            child.nextSibling.info = child.info;
         }
         child = child.nextSibling;
      }
      if (child1 != null){
         sb.append(tab.repeat(count-1));
      }
      sb.append("</L").append(count).append(">");
      if (count != 1) {
         sb.append("\n");
      }
      if (this.nextSibling != null){
         sb.append(tab.repeat(count-1));
      }

      return sb.toString();
   }

   public static void main (String[] param) {
      String s = "(((Gg, Hhh, (Zzz, (Www)Xx)Qq)Dd, Eee, (Ii)Ff)Bb, (Jj, Kkk)Cc)Aa";
      Node t = Node.parsePostfix (s);
      String v = t.leftParentheticRepresentation();
      System.out.println("");
      System.out.println (s + " ===++++===> " + v);
      System.out.println("");
      String q = t.pseudoXML();
      System.out.println(q);
   }
}

